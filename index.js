const express = require('express')
const bodyParser = require('body-parser')
const http = require('http')
const path = require('path')
const fs = require('fs')


const NODE_ENV = process.env.NODE_ENV || 'development'
const PORT = process.env.PORT || 80

const app = express()
const server = http.createServer(app)

app.use(bodyParser.json({ limit: "4mb" }))
app.use(bodyParser.urlencoded({ extended: false }))
app.use('/', express.static(path.join(__dirname, './public')));

app.post('/', (req, res) => {
  const user = new Date().toISOString() + ' ' + req.body.name + ' ' + req.body.email + '\n'

  fs.appendFile('mailingList.txt', user, (err) => {
    if (err) throw err
  })

  return res.status(200).end()
})

server.listen(PORT, () =>
  console.log('start in ' + NODE_ENV + ' environment on port ' + PORT)
)
